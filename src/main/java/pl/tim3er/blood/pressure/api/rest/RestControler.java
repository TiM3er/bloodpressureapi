package pl.tim3er.blood.pressure.api.rest;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.tim3er.blood.pressure.api.dao.BloodPressureDao;
import pl.tim3er.blood.pressure.api.dbconnector.IPressure;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

@RestController
public class RestControler {
    int timeMinus = 2 * 60 * 60 * 1000;
    @Autowired
    IPressure iPressure;

    @GetMapping("/bloodPressureList")
    public List<BloodPressureDao> bloodPressureList() {


        List<BloodPressureDao> bloodPressureDaos = iPressure.getAllBloodPressures();
        for (BloodPressureDao pressureDao : bloodPressureDaos) {
            pressureDao.setData(new Timestamp(pressureDao.getData().getTime() + timeMinus));
        }
        return bloodPressureDaos;
    }

    @PostMapping(value = "/bloodPressureAdd", produces = "application/json")
    public boolean bloodPressureAdd(@RequestBody String json) {
        System.out.println(json);
        try {
            ObjectMapper mapper = new ObjectMapper();
            BloodPressureDao bloodPressureDao = mapper.readValue(json, BloodPressureDao.class);

            Timestamp timestamp = new Timestamp(bloodPressureDao.getData().getTime() - timeMinus);
            bloodPressureDao.setData(timestamp);
            iPressure.addNewBloodPressure(bloodPressureDao);
            System.out.println(bloodPressureDao.toString());
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }


    }
}
