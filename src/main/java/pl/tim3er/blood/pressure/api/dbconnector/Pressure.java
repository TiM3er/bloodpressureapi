package pl.tim3er.blood.pressure.api.dbconnector;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;
import pl.tim3er.blood.pressure.api.dao.BloodPressureDao;


@Repository
public class Pressure implements IPressure {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public List<BloodPressureDao> getAllBloodPressures() {
        List<BloodPressureDao> bloodPressureDaos = new LinkedList<>();
        String sql = "SELECT data,systolic,diastolic,pulse,medicine,effort FROM pressuere  ORDER BY data DESC;";
        bloodPressureDaos = jdbcTemplate.query(sql, new ResultSetExtractor<List<BloodPressureDao>>() {
            @Nullable
            @Override
            public List<BloodPressureDao> extractData(ResultSet resultSet) throws SQLException {
                List<BloodPressureDao> daos = new LinkedList<>();
                while (resultSet.next()) {
                    daos.add(new BloodPressureDao(resultSet.getTimestamp(1), resultSet.getInt(2),
                            resultSet.getInt(3), resultSet.getInt(4),resultSet.getBoolean(5),resultSet.getString(6)));
                }

                return daos;
            }
        });

        return bloodPressureDaos;
    }


    @Override
    public void addNewBloodPressure(BloodPressureDao bloodPressureDao) {
        jdbcTemplate.update("insert into pressuere (data,systolic,diastolic,pulse, medicine, effort) values (to_timestamp(?,'yyyy-mm-ddThh24:mi:ss'),?,?,?,?,?);",
                bloodPressureDao.getData(), bloodPressureDao.getSystiolic(), bloodPressureDao.getDiastiloc(), bloodPressureDao.getPulse(),bloodPressureDao.isMedicine(),bloodPressureDao.getEffort());
    }

}
