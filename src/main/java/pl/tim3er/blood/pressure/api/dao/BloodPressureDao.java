package pl.tim3er.blood.pressure.api.dao;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@Data
@NoArgsConstructor
public class BloodPressureDao {
    @JsonProperty("data")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    Timestamp data;
    @JsonProperty("systiolic")
    int systiolic;
    @JsonProperty("diastiloc")
    int diastiloc;
    @JsonProperty("pulse")
    int pulse;
    @JsonProperty("medicine")
    boolean medicine;
    @JsonProperty("effort")
    String effort;


}
