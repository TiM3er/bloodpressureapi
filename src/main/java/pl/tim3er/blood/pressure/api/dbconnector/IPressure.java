package pl.tim3er.blood.pressure.api.dbconnector;

import pl.tim3er.blood.pressure.api.dao.BloodPressureDao;

import java.util.List;

public interface IPressure {
    List<BloodPressureDao>  getAllBloodPressures();
    void addNewBloodPressure(BloodPressureDao bloodPressureDao);
}
